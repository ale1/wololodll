﻿using System;
using System.Net;
using System.Net.Http;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Threading.Tasks;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;

namespace WololoDLL
{

    public class SimpleWebConnector
    {
      
        private static SimpleWebConnector _instance;
        public static SimpleWebConnector Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SimpleWebConnector();
                }

                return _instance;
            }
        }

        readonly string baseURL = "https://api.wololoci.com/api/";
        readonly string jobURL = "https://api-dev.wololoci.com/api/jobs/dummy-job/";
        readonly string pingURL = "ping/";


        public List<WoloJob> ConnectToJobs()
        {
            List<WoloJob> jobs = new List<WoloJob>();

            JSONArray jarray = (JSONArray) JSON.Parse(Get(jobURL));

            for (int i= 0; i< jarray.Count; i++)
            {
               
                jobs.Add(new WoloJob(jarray[i]));
            }

            Debug.Log("jobs found: " + jobs.Count);
            return jobs;
        }


        public string ConnectAsyn()
        {
            return GetAsync(baseURL + pingURL).Result;
        }


        private async Task<string> GetAsync(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return await reader.ReadToEndAsync();
            }
        }


        private string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }


    }
}