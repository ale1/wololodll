﻿using System;
using System.IO;
using System.Reflection;
using UnityEngine;
using SimpleJSON;

namespace WololoDLL
{

    public class WoloJob
    {
        public int id;
        public string state;
        public string urlBuild;
        public string urlLogs;
        public string urlSummary;
        public string urlIpa;
        public string urlXcodeLog;
        public string buildBranch;
        public int totalTime;
        public string buildEvent;
        public string createdAt;
        public string buildConfiguration;
        public int projectId;
        public string branch;
        public string buildName;

        public WoloJob(JSONNode json)
        {
            id = json["id"].AsInt;
            state = json["state"];
            urlBuild = json["urlBuild"];
            urlLogs = json["urlLogs"];
            buildBranch = json["buildBranch"];
            totalTime = json["totalTime"].AsInt;
            buildName = json["buildConfiguration"]["buildName"];

            //Debug.Log("poop" + buildName);
            //Debug.Log(json["buildConfiguration"]);

        }
   
    }
}

