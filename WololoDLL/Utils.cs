﻿using System;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace WololoDLL
{
    public class Utils
    {
        public static Texture2D LoadTextureFromDLL(string filename)
        {
            Texture2D newTex = new Texture2D(2, 2);
            Assembly _assembly = Assembly.GetExecutingAssembly();

            //this if statement is for detecting when this function is called from within original Unity project
            //if (_assembly.CodeBase.Contains("Assembly-CSharp-Editor.dll"))
            // {
            //     Debug.Log("Loading std asset");
            //     return AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/resources/icons/" + filename);
            // }
            //if we get here, this is being called as a DLL, extract texture
            Stream _imageStream = null;
            try
            {
                _imageStream = _assembly.GetManifestResourceStream("WololoDLL." + filename);// this is the namespace this function lives in.
            }
            catch
            {
                Debug.LogWarning("Unable to find " + filename + " resource in DLL " + _assembly.FullName);
                return newTex;
            }
            if (_imageStream == null)//sanity check- should be "caught" above
            {
                Debug.LogWarning("Unable to find " + filename + " resource in DLL " + _assembly.FullName);
                return newTex;
            }
            byte[] imageData = new byte[_imageStream.Length];
            _imageStream.Read(imageData, 0, (int)_imageStream.Length);

            if (!newTex.LoadImage(imageData))
                Debug.LogWarning("Unable to Load " + filename + " resource from DLL" + _assembly.FullName);
            return newTex;
        }

        public static T RandomFromEnum<T>()
        {
            Array values = Enum.GetValues(typeof(T));
            System.Random random = new System.Random();
            T randomItem = (T)values.GetValue(random.Next(values.Length));
            return randomItem;
        }

    }
}

