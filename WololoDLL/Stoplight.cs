﻿
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using System;

namespace WololoDLL
{
    public class StoplightView : EditorWindow
    {

        #region model

        public enum lightColor
        {
            GREEN,
            YELLOW,
            RED
        }

        public lightColor status;

        public List<string> Errors = new List<string>();
        public List<string> Warnings = new List<string>();


        public void Test()
        {
            if (DebugReport.report == null)
                Debug.LogError("no debug report found");


            //chanchada
            Dictionary<string, string> report = WololoDLL.DebugReport.report;

            

            Errors.Clear();
            Warnings.Clear();


            if (report["UnitySplash"] != "0")
                Warnings.Add("WARNING: you are building with unity splash enabled");

            if (Errors.Count > 0)
                SetLight(lightColor.RED);
            else if (Warnings.Count > 0)
                SetLight(lightColor.YELLOW);
            else
                SetLight(lightColor.GREEN);
        }

        #endregion model



        #region view

        public string statusText;

        //Adds menu item to unity toolbar
        [MenuItem("Window/Wololo/Stoplight")]
        public static void ShowWindow()
        {
            //Show Existing Window. if it doesnt exist, it will create one.
            EditorWindow.GetWindow(typeof(StoplightView), false, "Wololo Stoplight");
        }



        void OnEnable()
        {

            
        }

        bool showExample=false;
        private void OnGUI()
        {
            EditorGUILayout.TextField("STOPLIGHT STATUS: ", statusText);

            if (GUILayout.Button("Test"))
            {
                OnTestButtonPressed();

                showExample = true;
            
            }


            if(showExample)
            {
                GUILayout.Label("WARNING: you are building with unity splash enabled");
                string[] options1 = {"yes, please build with Unity Splash", "<AutoResolve> Ignore setting and build without splash"};
                GUILayout.SelectionGrid(0, options1, 1);

                GUILayout.Label("---------------------------");
                GUILayout.Label("WARNING: your local build number is 1.1.2 but last build was 1.1.5");
                GUILayout.Button("yes, use build number 1.1.2 please");
                GUILayout.Button("<AutoResolve> Use suggested 1.1.6 instead");
                GUILayout.Button("Oops, show me where to change it");

                GUILayout.Label("---------------------------");
                GUILayout.Label("WARNING: you have buildScriptsOnly enabled");
                GUILayout.Button("Yes, I want script-only build");
                GUILayout.Button("<AutoResolve>Oops, please fix it for me");
                GUILayout.Button("Oops, show me where to change it");

            }

            this.Repaint();





        }

        public void OnTestButtonPressed()
        {
            Test();
        }

        public void SetLight(lightColor color)
        {
            if (color == lightColor.RED)
                statusText = "RED";
            if (color == lightColor.YELLOW)
                statusText = "YELLOW";
            else
                statusText = "GREEN";


        }

        #endregion view


    }



}
