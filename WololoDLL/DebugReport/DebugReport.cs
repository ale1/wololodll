﻿
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;

namespace WololoDLL
{
    public class DebugReport : EditorWindow
    {

        #region Model
        //todo: crear una clase ReportEntry
        public static Dictionary<string,string> report = new Dictionary<string, string>();

        #endregion Model;



        public string ReportText = "";

        //Adds menu item to unity toolbar
        [MenuItem("Window/Wololo/DebugReport")]
        public static void ShowWindow()
        {
            //Show Existing Window. if it doesnt exist, it will create one.
            EditorWindow.GetWindow(typeof(DebugReport), false, "Wololo Debug Report");
        }
        

        void OnEnable()
        {

       
        }

        private void OnGUI()
        {
           

            if (GUILayout.Button("GenerateReport"))
            {
                GenerateReport();
                ReportText = "";
                foreach (KeyValuePair<string, string> item in report)
                {
                    ReportText += item.Key + ":  " + item.Value + " \n";
                }
                

            }
            GUILayout.FlexibleSpace();

            
            GUILayout.Label(ReportText);

            
            this.Repaint();





        }

 

        public void GenerateReport()
        {

            RepositoryInformation RepoInfo = RepositoryInformation.GetRepositoryInformation();

            report.Clear();

            report.Add("UserID", CloudProjectSettings.userId != null ? CloudProjectSettings.userId : "none");
            report.Add("userName", CloudProjectSettings.userName);

            report.Add("GitBranch", RepoInfo.BranchName);
            report.Add("GitCommit", RepoInfo.CommitHash);
            report.Add("GitTrackedBranch", RepoInfo.TrackedBranchName);
            report.Add("GitDirty", RepoInfo.HasUncommittedChanges.ToString());

            report.Add("ReportDate", System.DateTime.Now.ToString());

            report.Add("UnityVersion: ", Application.unityVersion);
            report.Add("Full Unity Version", InternalEditorUtility.GetFullUnityVersion());
            report.Add("HasLicence",  UnityEditorInternal.InternalEditorUtility.HasPro().ToString());
            

           // System.DateTime refDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
           // string VersionDate = refDate.AddSeconds(InternalEditorUtility.GetUnityVersionDate()).ToString();

           report.Add("localOS", SystemInfo.operatingSystem);
           report.Add("OSFamily", SystemInfo.operatingSystemFamily.ToString());

           report.Add ("ApplicationIdentifier", PlayerSettings.applicationIdentifier);
           report.Add("activeBuildTargetGroup", EditorUserBuildSettings.selectedBuildTargetGroup.ToString());
           report.Add("activeBuildTarget", EditorUserBuildSettings.activeBuildTarget.ToString());
           report.Add("usingAppBundle", EditorUserBuildSettings.buildAppBundle.ToString());
           report.Add("BuildScriptsOnly", EditorUserBuildSettings.buildScriptsOnly.ToString());
                
           report.Add("ScenesIncluded", EditorBuildSettings.scenes.ToString());

           report.Add("bundleVersion", PlayerSettings.bundleVersion.ToString());
           report.Add("AndroidbundleVersionCode",PlayerSettings.Android.bundleVersionCode.ToString());
           report.Add("ApplebuildNumber", PlayerSettings.iOS.buildNumber);

           report.Add("UnitySplash",PlayerSettings.SplashScreen.showUnityLogo.ToString());

            report.Add("AndroidBuildTarget", EditorUserBuildSettings.androidBuildSystem.ToString());
            report.Add("Texture Compression", EditorUserBuildSettings.androidBuildSubtarget.ToString()); 
         

        }



    }
}
