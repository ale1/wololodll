using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;


namespace WololoDLL
{

	static class MyTreeElementGenerator
	{
		static int IDCounter;
		static int minNumChildren = 4;
		static int maxNumChildren = 10;
		static float probabilityOfBeingLeaf = 0.5f;


        public static List<MyTreeElement> GenerateTreeWithData()
        {
            List<WoloJob> jobs = SimpleWebConnector.Instance.ConnectToJobs();

            //todo: separar en android, mac y webgl jobs.
            
            IDCounter = 0;
            var treeElements = new List<MyTreeElement>();

            var root = new MyTreeElement("Root", -1, IDCounter, null);
            treeElements.Add(root);

            var category1 = new MyTreeElement("ANDROID ", 0, ++IDCounter, null);
            treeElements.Add(category1);

            foreach (WoloJob job in jobs)
            {
                treeElements.Add(new MyTreeElement(job.buildName, 1, ++IDCounter, job));
            }

            var category2 = new MyTreeElement("IOS", 0, ++IDCounter, null);
            treeElements.Add(category2);

            var category3 = new MyTreeElement("WebGL", 0, ++IDCounter, null);
            treeElements.Add(category3);

            return treeElements;

        }

	}
}
