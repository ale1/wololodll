﻿using System;
using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using System.IO;
using System.Reflection;
using UnityEditor;

namespace WololoDLL
{
    class SimpleTreeView : TreeView
    {


        public SimpleTreeView(TreeViewState treeViewState)
            : base(treeViewState)
        {
            Reload();
        }

        protected override TreeViewItem BuildRoot()
        {

            // BuildRoot is called every time Reload is called to ensure that TreeViewItems 
            // are created from data. Here we create a fixed set of items. In a real world example,
            // a data model should be passed into the TreeView and the items created from the model.

            // This section illustrates that IDs should be unique. The root item is required to 
            // have a depth of -1, and the rest of the items increment from that.
            var root = new TreeViewItem { id = 0, depth = -1, displayName = "Root" };

            var IOS = new TreeViewItem { id = 1, depth = 0, displayName = "IOS" };
            IOS.icon = Utils.LoadTextureFromDLL("Icons.IOS_icon.png");

            var ANDROID = new TreeViewItem { id = 4, depth = 0, displayName = "Android" };
            ANDROID.icon = Utils.LoadTextureFromDLL("Icons.Android_icon.png");

            var PC = new TreeViewItem { id = 7, depth = 0, displayName = "PC" };
            PC.icon = Utils.LoadTextureFromDLL("Icons.PC_icon.png");

            var allItems = new List<TreeViewItem>
            {
            IOS,
            new TreeViewItem {id = 2, depth = 1, displayName = "AgeOfEmpires -- Status:  PROCURING"},
            new TreeViewItem {id = 3, depth = 1, displayName = "Starcraft -- Status: OK"},
            ANDROID,
            new TreeViewItem {id = 5, depth = 1, displayName = "AgeOfEmpires -- Status: ERROR!"},
            new TreeViewItem {id = 6, depth = 1, displayName = "Starcraft -- Status: OK!"},
            PC,
            new TreeViewItem {id=8, depth = 1, displayName = "No Standalone builds!"}
        };

            // Utility method that initializes the TreeViewItem.children and .parent for all items.
            SetupParentsAndChildrenFromDepths(root, allItems);

            // Return root of the tree
            return root;
        }        
    }
}
