using System;
using UnityEngine;
using Random = UnityEngine.Random;


namespace WololoDLL
{

	[Serializable]
	internal class MyTreeElement : TreeElement
	{
        public bool isJob
        {
            get
            {
                return Job != null;
            }
        }
     

        public WoloJob Job;
        public BuildStatus Buildstatus;
        public Plataform Plataform;
		public string Notes;
		public bool enabled;
        public string EngineVersion;

		public MyTreeElement (string name, int depth, int id, WoloJob job) : base (name, depth, id)
		{
            Job = job;
			enabled = true;
            
            Notes = "";
            EngineVersion = "-banana-";

            SetupJobVars();

		}

        private void SetupJobVars()
        {
            if (Job == null)    //los caret lines en la tabla ("IOS, Android, webgl"  cuentan como treeelements, pero estos no tienen jobs)
                return;

            if (Job.state == "success")
                Buildstatus = BuildStatus.OK;
            else
                Buildstatus = BuildStatus.Unknown;

            Plataform = Plataform.ANDROID; // todo: quitar hardcodeado
        }
	}

    public enum BuildStatus
    {
        OK = 0,               //green
        Error = 1,            //Red  
        Procuring = 2,        // blue
        Unknown = 3           // grey
    }

    public enum Plataform
    {
        IOS = 0,
        ANDROID = 1,
        WEBGL = 2
    }

    
}
