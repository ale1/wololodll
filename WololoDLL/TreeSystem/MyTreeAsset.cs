using System.Collections.Generic;
using UnityEngine;

namespace WololoDLL
{
	
	[CreateAssetMenu (fileName = "WololoSavedData", menuName = "Wololo Tree Scriptable Object", order = 1)]
	public class MyTreeAsset : ScriptableObject
	{
		[SerializeField] List<MyTreeElement> m_TreeElements = new List<MyTreeElement> ();

		internal List<MyTreeElement> treeElements
		{
			get { return m_TreeElements; }
			set { m_TreeElements = value; }
		}


        public void FillWithWebData()
        {
            m_TreeElements = MyTreeElementGenerator.GenerateTreeWithData();
        }
	}
}
