using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.IMGUI.Controls;
using UnityEngine;


namespace WololoDLL
{

	class MultiColumnWindow : EditorWindow
	{
		[NonSerialized] bool m_Initialized;
		[SerializeField] TreeViewState m_TreeViewState; // Serialized in the window layout file so it survives assembly reloading
		[SerializeField] MultiColumnHeaderState m_MultiColumnHeaderState;
		SearchField m_SearchField;
		MultiColumnTreeView m_TreeView;
		public MyTreeAsset m_MyTreeAsset { get; private set; }

		[MenuItem("Window/Wololo/Advanced Dashboard")]
        public static MultiColumnWindow LoadWindow()
        {
            var guid = AssetDatabase.FindAssets("t:MyTreeAsset");

            if(guid.Length > 0)
            { 
                MyTreeAsset myTreeAsset = AssetDatabase.LoadAssetAtPath<MyTreeAsset>(AssetDatabase.GUIDToAssetPath(guid[0]));
                var window = GetWindow();
                window.SetTreeAsset(myTreeAsset);
                Debug.Log("opened scriptableObject");
                return window;
            }
            else
            {
                Debug.Log("no saved tree found");
                return GetWindow();
            }
        }


        private static MultiColumnWindow GetWindow ()
		{
            
			var window = GetWindow<MultiColumnWindow>();
			window.titleContent = new GUIContent("Wololo Dashboard");
			window.Focus();
			window.Repaint();
			return window;
		}



		[OnOpenAsset]
		public static bool OnOpenAsset (int instanceID, int line)
		{
			var myTreeAsset = EditorUtility.InstanceIDToObject (instanceID) as MyTreeAsset;
			if (myTreeAsset != null)
			{
				var window = GetWindow ();
				window.SetTreeAsset(myTreeAsset);
                Debug.Log("opened tree Asset");
				return true;
			}
            Debug.Log("failed to handle open of tree" );
			return false; // we did not handle the open
		}

		void SetTreeAsset (MyTreeAsset myTreeAsset)
		{
			m_MyTreeAsset = myTreeAsset;
			m_Initialized = false;
		}

		Rect multiColumnTreeViewRect
		{
			get { return new Rect(20, 40, position.width-40, position.height-70); }
		}

		Rect searchbarRect
		{
			get { return new Rect (20f, 20f, position.width-40f, 20f); }
		}

        Rect toolbarRect
        {
            get { return new Rect(0, 0, position.width, 20f); }
        }

		Rect bottomToolbarRect
		{
			get { return new Rect(20f, position.height - 18f, position.width - 40f, 18f); }
		}

		public MultiColumnTreeView treeView
		{
			get { return m_TreeView; }
		}

        public int PageNum = 0;

		void InitIfNeeded ()
		{
			if (!m_Initialized)
			{
				// Check if it already exists (deserialized from window layout file or scriptable object)
				if (m_TreeViewState == null)
					m_TreeViewState = new TreeViewState();

				var headerState = MultiColumnTreeView.CreatePageMultiColumnHeaderState(PageNum, multiColumnTreeViewRect.width);
				if (MultiColumnHeaderState.CanOverwriteSerializedFields(m_MultiColumnHeaderState, headerState))
					MultiColumnHeaderState.OverwriteSerializedFields(m_MultiColumnHeaderState, headerState);
				m_MultiColumnHeaderState = headerState;
				
				var multiColumnHeader = new MyMultiColumnHeader(headerState);
				multiColumnHeader.ResizeToFit ();

				var treeModel = new TreeModel<MyTreeElement>(GetData());
				
				m_TreeView = new MultiColumnTreeView(PageNum,m_TreeViewState, multiColumnHeader, treeModel);

				m_SearchField = new SearchField();
				m_SearchField.downOrUpArrowKeyPressed += m_TreeView.SetFocusAndEnsureSelectedItem;

				m_Initialized = true;
			}
		}
		
		IList<MyTreeElement> GetData ()
		{
   
                //generate new treeasset with fetched data.
                m_MyTreeAsset = ScriptableObject.CreateInstance<MyTreeAsset>();
                Debug.Log("tree asset was empty, so fetching data");
                m_MyTreeAsset.FillWithWebData();

            return m_MyTreeAsset.treeElements;


        }

        public void Reset()
        {
            m_Initialized = false;
            GetWindow().Repaint();
        }

		void OnSelectionChange ()
		{
			if (!m_Initialized)
				return;

			var myTreeAsset = Selection.activeObject as MyTreeAsset;
			if (myTreeAsset != null && myTreeAsset != m_MyTreeAsset)
			{
				m_MyTreeAsset = myTreeAsset;
				m_TreeView.treeModel.SetData (GetData ());
				m_TreeView.Reload ();
			}
		}

		void OnGUI ()
		{
			InitIfNeeded();

            TopToolBar(toolbarRect);
			SearchBar (searchbarRect);
			DoTreeView (multiColumnTreeViewRect);
			BottomToolBar (bottomToolbarRect);
		}

		void SearchBar (Rect rect)
		{
			treeView.searchString = m_SearchField.OnGUI (rect, treeView.searchString);
		}

		void DoTreeView (Rect rect)
		{
			m_TreeView.OnGUI(rect);
		}

        void TopToolBar(Rect rect)
        {
            GUILayout.BeginHorizontal(EditorStyles.toolbar);

            if(GUILayout.Button("Refresh", EditorStyles.toolbarButton))
            {
                OnMenu_Refresh();
                EditorGUIUtility.ExitGUI();
            }

            if (GUILayout.Button("Save", EditorStyles.toolbarButton))
            {
                OnMenu_Save();
                EditorGUIUtility.ExitGUI();
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Tools", EditorStyles.toolbarDropDown))
            {
                GenericMenu toolsMenu = new GenericMenu();
                if (m_TreeView.GetSelection().Count > 0)
                    toolsMenu.AddItem(new GUIContent("Go to BuildPage"), false, OnTools_DoSomethingCool);
                else
                    toolsMenu.AddDisabledItem(new GUIContent("Do Something Cool"));
                toolsMenu.AddSeparator("");
                toolsMenu.AddItem(new GUIContent("Help..."), false, OnTools_Help);
                // Offset menu from right of editor window
                toolsMenu.DropDown(new Rect(Screen.width - 216 - 20, 0, 0, 16));
                EditorGUIUtility.ExitGUI();
            }

            GUILayout.EndHorizontal();
        }



		void BottomToolBar (Rect rect)
		{
			GUILayout.BeginArea (rect);

			using (new EditorGUILayout.HorizontalScope ())
			{

				var style = "miniButton";
				if (GUILayout.Button("Reset", style))
				{
                    Debug.Log("Reset!");
                    Reset();
				}

				if (GUILayout.Button("Something Else", style))
				{
                    Debug.Log("Did Something else!");
                    //treeView.CollapseAll ();
				}

				GUILayout.FlexibleSpace();

				GUILayout.Label (m_MyTreeAsset != null ? AssetDatabase.GetAssetPath (m_MyTreeAsset) : "no save data");

				GUILayout.FlexibleSpace ();
                GUILayout.Label("current Page:" +PageNum);
				GUILayout.Space (10);
				
				if (GUILayout.Button("<-- Prev Page", style))
				{
                    PageNum = Math.Max(0, PageNum - 1);
                    Reset();
				}

                if (GUILayout.Button("Next Page -->", style))
                {
                    PageNum = Math.Min(1, PageNum + 1);
                    Reset();
                }
            }

			GUILayout.EndArea();
		}


        void OnMenu_Save()
        {
            Debug.Log("save button pressed");
            AssetDatabase.CreateAsset(m_MyTreeAsset, "Assets/mysavedTree.asset");
            AssetDatabase.SaveAssets();
        }

        private void OnMenu_Refresh()
        {
           GetData();
           
        }


        void OnTools_DoSomethingCool()
        {

            int selectedID = m_TreeView.GetSelection()[0];
            string url = $"https://app.wololoci.com/downloads/{selectedID}";
            Help.BrowseURL(url);


        }
        void OnTools_Help()
        {
            Help.BrowseURL("http://www.wololoci.com/plugin/help");
        }


    }


    internal class MyMultiColumnHeader : MultiColumnHeader
	{
		Mode m_Mode;

		public enum Mode
		{
			LargeHeader,
			DefaultHeader,
			MinimumHeaderWithoutSorting
		}

		public MyMultiColumnHeader(MultiColumnHeaderState state): base(state)
		{
			mode = Mode.DefaultHeader;
		}

		public Mode mode
		{
			get
			{
				return m_Mode;
			}
			set
			{
				m_Mode = value;
				switch (m_Mode)
				{
					case Mode.LargeHeader:
						canSort = true;
						height = 37f;
						break;
					case Mode.DefaultHeader:
						canSort = true;
						height = DefaultGUI.defaultHeight;
						break;
					case Mode.MinimumHeaderWithoutSorting:
						canSort = false;
						height = DefaultGUI.minimumHeight;
						break;
				}
			}
		}

		protected override void ColumnHeaderGUI (MultiColumnHeaderState.Column column, Rect headerRect, int columnIndex)
		{
			// Default column header gui
			base.ColumnHeaderGUI(column, headerRect, columnIndex);

			// Add additional info for large header
			if (mode == Mode.LargeHeader)
			{
				// Show example overlay stuff on some of the columns
				if (columnIndex > 2)
				{
					headerRect.xMax -= 3f;
					var oldAlignment = EditorStyles.largeLabel.alignment;
					EditorStyles.largeLabel.alignment = TextAnchor.UpperRight;
					GUI.Label(headerRect, 36 + columnIndex + "%", EditorStyles.largeLabel);
					EditorStyles.largeLabel.alignment = oldAlignment;
				}
			}
		}
	}

}
